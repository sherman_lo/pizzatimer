//
//  ViewController.swift
//  PizzaTimer
//
//  Created by Sherman Lo on 1/12/2014.
//  Copyright (c) 2014 Sherman Lo. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view, typically from a nib.
	}

	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
		// Dispose of any resources that can be recreated.
	}

	@IBAction func sendNotification(button: UIButton) {
		let notification = UILocalNotification()
		notification.alertBody = "This is a notification"
		
		UIApplication.sharedApplication().presentLocalNotificationNow(notification)
	}
	
}

