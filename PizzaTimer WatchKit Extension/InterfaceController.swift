//
//  InterfaceController.swift
//  PizzaTimer WatchKit Extension
//
//  Created by Sherman Lo on 1/12/2014.
//  Copyright (c) 2014 Sherman Lo. All rights reserved.
//

import WatchKit
import Foundation


class InterfaceController: WKInterfaceController {

    override init(context: AnyObject?) {
        // Initialize variables here.
        super.init(context: context)
        
        // Configure interface objects here.
        NSLog("%@ init", self)
    }

    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
        NSLog("%@ will activate", self)
    }

    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        NSLog("%@ did deactivate", self)
        super.didDeactivate()
    }
	
	override func table(table: WKInterfaceTable, didSelectRowAtIndex rowIndex: Int) {
		println("Displayed \(rowIndex)")
	}
	
	override func handleActionWithIdentifier(identifier: String?, forRemoteNotification remoteNotification: [NSObject : AnyObject]) {
		if identifier! == "secondButtonAction" {
			self.pushControllerWithName("SwedishPizza", context: nil)
		}
	}
	
	@IBAction func italianPizza(button: WKInterfaceButton) {
//		let urlRequest = NSURLRequest(URL: NSURL(string: "http://www.google.com/")!)
//		let session = NSURLSession.sharedSession()
//		let task = session.dataTaskWithRequest(urlRequest, completionHandler: { (data: NSData!, response: NSURLResponse!, error: NSError!) -> Void in
//			
//			let string = NSString(data: data, encoding: NSUTF8StringEncoding)
//			println("Data \(string)")
//			
			self.pushControllerWithName("ItalianPizza", context: nil)
//		})
		
//		task.resume()
	}
	
	@IBAction func swedishPizza(button: WKInterfaceButton) {
//		let urlRequest = NSURLRequest(URL: NSURL(string: "http://www.google.com/")!)
//		let session = NSURLSession.sharedSession()
//		let task = session.dataTaskWithRequest(urlRequest, completionHandler: { (data: NSData!, response: NSURLResponse!, error: NSError!) -> Void in
//			
//			let string = NSString(data: data, encoding: NSUTF8StringEncoding)
//			println("Data \(string)")
//			
			self.pushControllerWithName("SwedishPizza", context: nil)
//		})
		
//		task.resume()
	}
	
}
